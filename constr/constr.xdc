create_clock -period 2.500 -name clk -waveform {0.000 1.250} [get_ports -filter { NAME =~  "*clk*" && DIRECTION == "IN" }]
set_input_delay  -clock [get_clocks *] 0.500 [get_ports -filter { NAME =~  "*" && NAME !~  "*clk*" && DIRECTION == "IN" }]
set_output_delay -clock [get_clocks *] 0.500 [get_ports -filter { NAME =~  "*" && DIRECTION == "OUT" }]
