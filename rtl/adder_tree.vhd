
library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
use work.utils_pack.all;

entity adder_tree is
  	generic (
  			BW_DIN 			: integer ;
		  	N_COEF  		: integer ;
        BW_SUM      : integer 
	);
    Port ( 	clk : in STD_LOGIC;
           	rst : in STD_LOGIC;
           	
           	i_bus     : in STD_LOGIC_VECTOR  (N_COEF*BW_DIN-1 downto 0);
            o_sum     : out STD_LOGIC_VECTOR( BW_SUM-1 downto 0)
           	
		 	);
begin
--  constant N_STAGES: natural:= clog2(N_COEF);
--  assert ( BW_SUM = BW_DIN+N_STAGES) report "To avoid overflow adder output should be [BWDIN] + [N levels of tree] bit" severity failure;
end entity;
architecture Behavioral of adder_tree is

constant N_STAGES: natural:= clog2(N_COEF);
subtype din_t is STD_LOGIC_VECTOR(BW_DIN    -1 downto 0 );
type vec_inp_t    is array (N_COEF-1 downto 0) of din_t;
signal vec_input 	   : vec_inp_t		;


constant N_COEF_UP : natural := 2**N_STAGES;
type arr_expanded_t    is array (N_COEF_UP-1 downto 0) of din_t;
signal arr_expanded    : arr_expanded_t   :=(others=>(others=>'0'));

constant BW_VEC_EXPANDED : integer:= N_COEF_UP*BW_DIN;
signal vec_expanded  : STD_LOGIC_VECTOR(BW_VEC_EXPANDED   -1 downto 0 );
signal vec_expanded0 : STD_LOGIC_VECTOR(BW_VEC_EXPANDED/2 -1 downto 0 );
signal vec_expanded1 : STD_LOGIC_VECTOR(BW_VEC_EXPANDED/2 -1 downto 0 );

constant HALF_COEF : natural:= N_COEF_UP/2;
signal dbg2 : STD_LOGIC_VECTOR(HALF_COEF-1 downto 0);


type arr_st5_t is array (2**5-1 downto 0) of unsigned(BW_DIN + 0 -1 downto 0);
type arr_st4_t is array (2**4-1 downto 0) of unsigned(BW_DIN + 1 -1 downto 0);
type arr_st3_t is array (2**3-1 downto 0) of unsigned(BW_DIN + 2 -1 downto 0);
type arr_st2_t is array (2**2-1 downto 0) of unsigned(BW_DIN + 3 -1 downto 0);
type arr_st1_t is array (2**1-1 downto 0) of unsigned(BW_DIN + 4 -1 downto 0);
type arr_st0_t is array (2**0-1 downto 0) of unsigned(BW_DIN + 5 -1 downto 0);

signal  uresult : unsigned(BW_DIN + 5 -1 downto 0);
signal  arr_st5 : arr_st5_t:=(others=>(others=>'0'));
signal  arr_st4 : arr_st4_t:=(others=>(others=>'0'));
signal  arr_st3 : arr_st3_t:=(others=>(others=>'0'));
signal  arr_st2 : arr_st2_t:=(others=>(others=>'0'));
signal  arr_st1 : arr_st1_t:=(others=>(others=>'0'));
signal  arr_st0 : arr_st0_t:=(others=>(others=>'0'));


signal sum_up  ,
       sum_low : STD_LOGIC_VECTOR( BW_SUM-1-1 downto 0);

begin

  G_input: for I in 0 to N_COEF-1 generate
    vec_input(I)   <= i_bus( (I+1)*BW_DIN-1 downto I*BW_DIN);
    arr_expanded(I)<= vec_input(I);
    arr_st5(I)<= unsigned(arr_expanded(I));
 end generate;

process (clk) begin
  if rising_edge(clk) then
    for I in 0 to (2**4)-1 loop arr_st4(I)<= resize(arr_st5(2*I),BW_DIN + 1) + resize(arr_st5(2*I+1),BW_DIN + 1); end loop;
    for I in 0 to (2**3)-1 loop arr_st3(I)<= resize(arr_st4(2*I),BW_DIN + 2) + resize(arr_st4(2*I+1),BW_DIN + 2); end loop;
    for I in 0 to (2**2)-1 loop arr_st2(I)<= resize(arr_st3(2*I),BW_DIN + 3) + resize(arr_st3(2*I+1),BW_DIN + 3); end loop;
    for I in 0 to (2**1)-1 loop arr_st1(I)<= resize(arr_st2(2*I),BW_DIN + 4) + resize(arr_st2(2*I+1),BW_DIN + 4); end loop;
    for I in 0 to (2**0)-1 loop arr_st0(I)<= resize(arr_st1(2*I),BW_DIN + 5) + resize(arr_st1(2*I+1),BW_DIN + 5); end loop;
  end if;
end process;
uresult<= arr_st0(0);
o_sum  <= std_logic_vector(uresult);

----------------------------------------------------------------------
-- Recursive Call doesn't work properly, generics never changes  
------------------------------------------------------------------------------

  --G_vectorize: for I in 0 to N_COEF-1 generate
  --   vec_expanded( (I+1)*BW_DIN-1 downto I*BW_DIN) <=  arr_expanded(I);
  --end generate;

  -- vec_expanded0<=vec_expanded(BW_VEC_EXPANDED/2 -1 downto                 0);
  -- vec_expanded1<=vec_expanded(BW_VEC_EXPANDED-1    downto BW_VEC_EXPANDED/2);

    --RETURN_SELF: if N_COEF=1 generate
    --      o_sum<= vec_input(0);
    --end generate RETURN_SELF;

    --SUB_UP: if N_COEF>2 generate
    --  adder_tree_up : entity work.adder_tree
    --      generic map (
    --          BW_DIN => BW_DIN,
    --          N_COEF => HALF_COEF-1,
    --          BW_SUM => BW_SUM-1
    --      )
    --      port map (
    --          clk   => clk,
    --          rst   => rst,
    --          i_bus => vec_expanded0,
    --          o_sum => sum_up
    --      );
    --end generate;

  --  SUB_LOW: if N_COEF>2 generate
  --  adder_tree_low : entity work.adder_tree
  --        generic map (
  --            BW_DIN => BW_DIN,
  --            N_COEF => HALF_COEF,
  --            BW_SUM => BW_SUM-1
  --        )
  --        port map (
  --            clk   => clk,
  --            rst   => rst,
  --            i_bus => vec_expanded1,
  --            o_sum => sum_low
  --        );
  --  end generate SUB_LOW;
  --  SUM: if N_COEF>2 generate
  --      process (clk) begin
  --        if rising_edge(clk) then
  --          o_sum<= unsigned(sum_up) + unsigned(sum_low);
  --        end if;
  --      end process;
  --  end generate;




end architecture Behavioral;


