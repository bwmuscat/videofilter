library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use work.utils_pack.all;


entity conv_uns is
  	generic (
  			BW_A 			: integer:=  16 ;
  			BW_B			: integer:=   8 ;
  			BW_OUT          : integer:=  12 ;
		  	N_COEF  		: integer:=   5 ;
		  	RND_BITS        : integer:=   10 
 	);
    Port ( 	clk : in STD_LOGIC;
           	rst : in STD_LOGIC;
           	
           	i_valid : in STD_LOGIC;
           	i_a     : in STD_LOGIC_VECTOR  (N_COEF*BW_A-1 downto 0);
           	i_b     : in STD_LOGIC_VECTOR  (N_COEF*BW_B-1 downto 0);
           	
           	o_res   : out  STD_LOGIC_VECTOR  (BW_OUT-1 downto 0);
			o_valid : out STD_LOGIC
		 	);
end entity;
architecture Behavioral of conv_uns is

constant BW_PROD: integer:= BW_A + BW_B;


type arr_a_t    is array (N_COEF-1 downto 0) of STD_LOGIC_VECTOR(BW_A    -1 downto 0 );
type arr_b_t    is array (N_COEF-1 downto 0) of STD_LOGIC_VECTOR(BW_B    -1 downto 0 );
type arr_prod_t is array (N_COEF-1 downto 0) of STD_LOGIC_VECTOR(BW_PROD -1 downto 0 );


constant N_ADDER_STAGES : integer:=clog2(N_COEF);
constant BW_SUM 		: integer:= N_ADDER_STAGES + BW_PROD;

signal arr_a 	: arr_a_t		;
signal arr_b 	: arr_b_t		;
signal arr_prod : arr_prod_t	;
signal vec_prod : STD_LOGIC_VECTOR(N_COEF*BW_PROD-1 downto 0);


signal sum_raw       : STD_LOGIC_VECTOR(BW_SUM-1 downto 0);
constant BW_ROUNED   : integer:=BW_SUM-(RND_BITS);
signal sum_rounded   : unsigned(BW_ROUNED -1 downto 0);
constant MAX_SAT     : integer:= 2**BW_OUT-1;
signal sum_saturated : unsigned(BW_OUT    -1 downto 0);
signal rnd_add       : std_logic;
signal rnd_add_int   : natural range 0 to 1;

constant FUll_DELAY: integer:=2+N_ADDER_STAGES+2;
signal	valid_line: STD_LOGIC_VECTOR(FUll_DELAY-1 downto 0);

begin

process (clk) begin
	if rising_edge(clk) then
		valid_line<=valid_line(FUll_DELAY-2 downto 0) & i_valid;
	end if;
end process;
o_valid<= valid_line(FUll_DELAY-1);

process (clk) begin
	if rising_edge(clk) then
	for K in 0 to N_COEF-1 loop
  	   arr_a(K)<=  i_a( ( K+1)*BW_A-1 downto
  	   					    K*BW_A ) ;
  	   arr_b(K)<=  i_b( (K+1)*BW_B-1 downto 
  	   					   K*BW_B ) ;
	arr_prod(K)<= 	unsigned(arr_a(K)) * unsigned(arr_b(K))  ;

	end loop;
	end if;
end process;


VECT: for K in 0 to N_COEF-1 generate
	
	vec_prod((K+1)*BW_PROD-1 downto   K*BW_PROD  ) <= arr_prod(K);

end generate VECT;


u_adder_tree : entity work.adder_tree
    generic map (
        BW_DIN => BW_PROD,
        N_COEF => N_COEF,
        BW_SUM => BW_SUM
    )
    port map (
        clk   => clk,
        rst   => rst,
        i_bus => vec_prod,
        o_sum => sum_raw
    );
rnd_add<=  sum_raw(RND_BITS-1) when RND_BITS>0 else '0';
rnd_add_int <= 1 when rnd_add='1' else 0;
process (clk) begin
	if rising_edge(clk) then
	sum_rounded<= unsigned(sum_raw(BW_SUM-1 downto RND_BITS))  + rnd_add_int ;--- +rnd_add);
	
	sum_saturated<= (others=>'1') when sum_rounded>MAX_SAT else sum_rounded(BW_OUT-1 downto 0);

	end if;
end process;
o_res<=STD_LOGIC_VECTOR(sum_saturated);


end architecture Behavioral;


