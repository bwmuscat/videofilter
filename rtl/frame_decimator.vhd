library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_misc.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use work.utils_pack.all;

entity frame_decimator is
  	generic (
  			BW_DIN 			: integer:=  16 

 	);
    Port ( 	clk : in STD_LOGIC;
           	rst : in STD_LOGIC;
    
           	i_din 		: in STD_LOGIC_VECTOR (BW_DIN-1 downto 0);
			i_valid 	: in STD_LOGIC;
			i_h_sync 	: in STD_LOGIC;
			i_v_sync	: in STD_LOGIC;
	
		 	o_din 		: out STD_LOGIC_VECTOR (BW_DIN-1 downto 0);
			o_valid 	: out STD_LOGIC;
			o_h_sync 	: out STD_LOGIC;
			o_v_sync	: out STD_LOGIC
     );

end entity;

architecture beh of frame_decimator is

signal d_din 		:  STD_LOGIC_VECTOR (BW_DIN-1 downto 0);
signal d_valid 		:  STD_LOGIC;
signal d_h_sync 	:  STD_LOGIC;
signal d_v_sync		:  STD_LOGIC;

signal skip_line, skip_pix: STD_LOGIC;

begin
	process (clk) begin
		if rising_edge(clk) then

			d_din <= i_din;
			d_valid <= i_valid;
			d_h_sync <= i_h_sync;
			d_v_sync <= i_v_sync;

			if (i_h_sync='0') and (d_h_sync='1') then
				skip_line <= not(skip_line);
			end if;

			if i_v_sync='0' and i_v_sync='1' then
				skip_line <= '0';
				skip_pix  <= '0';
			end if;

			if i_valid='1' then
				skip_pix  <=not(skip_pix);
			end if;

		end if;

		if (rst) then
			skip_pix  <= '0';
			skip_line <= '0';
		end if;
	end process;

o_din 	 <=d_din    ;
o_valid  <=d_valid  and not(skip_pix);
o_h_sync <=d_h_sync	and not(skip_line);
o_v_sync <=d_v_sync	;

	
end architecture beh;