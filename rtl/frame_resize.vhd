----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03.08.2018 17:28:34
-- Design Name: 
-- Module Name: frame_resize - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_misc.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use work.utils_pack.all;

entity frame_resize is
  	generic (
  			BW_DIN 			: integer:=  16 ;
  			BW_FIR			: integer:=   8 ;
		  	FIR_WIDTH		: integer:=   5 ;
		  	BW_FIR_VEC      : integer:= 200 ; -- 5*5*8
		  	FRAME_WIDTH 	: integer:=	1000 ;
		  	FRAME_HEIGTH	: integer:= 100 
 	);
    Port ( 	clk : in STD_LOGIC;
           	rst : in STD_LOGIC;
           	i_din 		: in STD_LOGIC_VECTOR (BW_DIN-1 downto 0);
			i_valid 	: in STD_LOGIC;
			i_h_sync 	: in STD_LOGIC;
			i_v_sync	: in STD_LOGIC;
			i_fir_coef  : in STD_LOGIC_VECTOR  (BW_FIR_VEC-1 downto 0);
		 	o_din 		: out STD_LOGIC_VECTOR (BW_DIN-1 downto 0);
			o_valid 	: out STD_LOGIC;
			o_h_sync 	: out STD_LOGIC;
			o_v_sync	: out STD_LOGIC
           );
begin
	assert (BW_FIR_VEC =  BW_FIR*FIR_WIDTH*FIR_WIDTH) report "Wrong value of Fir Coef Vec width" severity failure;
	assert ((FIR_WIDTH mod 2)=1 ) report "Filter must be odd 3,5,7 to get center coefficients "  severity failure;
end entity frame_resize;

architecture Behavioral of frame_resize is
constant N_FIR_COEFF		: integer := FIR_WIDTH*FIR_WIDTH;
constant N_PAD				: integer := (FIR_WIDTH - 1)/2; -- Padding zeros, 2 for FIR 5
constant LINES_BUFFER_DEPTH : integer := N_PAD            ; -- Buffer depth, 3 for FIR 5

subtype din_t   is STD_LOGIC_VECTOR(BW_DIN-1 downto 0);
subtype coef_t  is STD_LOGIC_VECTOR(BW_FIR-1 downto 0);

type line_t 	is array (FRAME_WIDTH-1 		downto 0 ) of din_t;
type line_buf_t is array (LINES_BUFFER_DEPTH-1  downto 0 ) of line_t;
signal line_buf_ready : std_logic;

signal hline_buf : line_t   ;
signal lines_buf : line_buf_t;
signal lines_buf_valid : STD_LOGIC_VECTOR(LINES_BUFFER_DEPTH-1 	downto 0);
signal h_sync_d    : std_logic;
signal v_sync_d	   : std_logic;
signal line_end	   : std_logic;
signal frame_start : std_logic;

constant PAD_LINE_WIDTH: integer:= N_PAD+FRAME_WIDTH+N_PAD;
type linepad_t 		is array (	PAD_LINE_WIDTH  -1  downto 0 ) of din_t  ;
type linepad_buf_t 	is array (  FIR_WIDTH    	-1	downto 0 ) of linepad_t ;
signal linepad_buf : linepad_buf_t;

signal latch_linebuf 		 : std_logic;
signal cycle_shift_lines	 : std_logic;
signal linepad_add_line	 	 : std_logic;

signal din_d: STD_LOGIC_VECTOR(BW_DIN-1 downto 0);
signal valid_d: std_logic;
signal pad_wait_new_frame : std_logic;

signal cnt_horiz : natural range 0 to FRAME_WIDTH-1;

type fir_mat_din_t   is array(FIR_WIDTH-1 downto 0,FIR_WIDTH-1 downto 0 	) of din_t;
type fir_arr1d_din_t is array(FIR_WIDTH**2-1 downto 0 					) of din_t;
subtype fir_vec_din_t   is std_logic_vector(BW_DIN*N_FIR_COEFF-1 downto 0);

signal fir_mat_din 		: fir_mat_din_t;
signal fir_arr1d_din 	: fir_arr1d_din_t;
signal fir_vec_din 		: fir_vec_din_t;

signal fir_out		: din_t;

signal fir_ivalid, fir_ovalid : STD_LOGIC;

signal fir_h_sync : STD_LOGIC;
signal fir_v_sync : STD_LOGIC;

constant VSYNC_DEL : integer:= 37;
signal v_sync_del : std_logic_vector (VSYNC_DEL-1 downto 0 )	;


begin

edge_detect: process (clk) begin
	if rising_edge(clk) then
		h_sync_d<= i_h_sync;
		v_sync_d<= i_v_sync;
		din_d	<= i_din;	
		valid_d	<= i_valid;
		frame_start<= '1' when v_sync_d='0' and i_v_sync='0' else '0';
		line_end<='1' when 	h_sync_d='1' 
						and  valid_d='1' 
						and i_h_sync='0' else '0';
	end if;
end process;


line_buf_ctrl: process (clk) begin
	if rising_edge(clk) then
		if (valid_d='1') then
			hline_buf(FRAME_WIDTH-1)<=din_d;
			for I in FRAME_WIDTH-2 downto 0 loop
				hline_buf(I)<=hline_buf(I+1);
			end loop;
		end if;
		
		if (line_end='1') then
			lines_buf(      0) 		<= hline_buf;
			lines_buf_valid(0)	<= '1';
			for I in LINES_BUFFER_DEPTH-1 downto 1 loop
				lines_buf(I)<= lines_buf(I-1);
				lines_buf_valid(I)<=lines_buf_valid(I-1);
			end loop;
		end if;


		if rst='1' then
			lines_buf_valid<=(others=>'0');
		end if;
	end if;
end process;

line_buf_ready	<= and_reduce(lines_buf_valid) and line_end;

latch_linebuf	<= line_buf_ready and pad_wait_new_frame;

linepad_add_line<= not(pad_wait_new_frame) and line_buf_ready;

fsm: process (clk) begin
	if rising_edge(clk) then
		
		if latch_linebuf='1' then
			pad_wait_new_frame<='0';
		end if;

		if rst='1' then
			pad_wait_new_frame<='1';
		end if;
	end if;
end process;

shift_ctrl: process(clk) begin
	if rising_edge(clk) then
		if (cycle_shift_lines='0') then
			if (latch_linebuf='1') OR (linepad_add_line='1') then
				cycle_shift_lines<='1';
				cnt_horiz<= FRAME_WIDTH-1;
			end if;
		else
			if (cnt_horiz=0) then
				cycle_shift_lines<='0';
			else
				cnt_horiz<=cnt_horiz-1;
			end if;
		end if;
		if rst='1' then
			cycle_shift_lines<='0';
		end if;

	end if;
end process;

linepad_ctrl : process(clk) begin
	if rising_edge(clk) then
		--------------------------------------------
		if (latch_linebuf='1') then
		--------------------------------------------
			-- Either latch buffer, fanout+1
		    for P in 0 to N_PAD-1 loop
				linepad_buf(0)(P                 )<=(others=>'0');-- here may be more interesting padding
				linepad_buf(0)(PAD_LINE_WIDTH-1-P)<=(others=>'0');-- here may be more interesting padding
			end loop;
			
			for S in FRAME_WIDTH-1 downto 0 loop
				linepad_buf(0)(S+N_PAD)<= hline_buf(S);	
			end loop;

			for L in LINES_BUFFER_DEPTH-1 downto 0 loop
			    for P in 0 to N_PAD-1 loop
					linepad_buf(L+1)(P                 )<=(others=>'0');-- here may be more interesting padding
					linepad_buf(L+1)(PAD_LINE_WIDTH-1-P)<=(others=>'0');-- here may be more interesting padding
				end loop;
				
				 for S in FRAME_WIDTH-1 downto 0 loop
					linepad_buf(L+1)(S+N_PAD)<= lines_buf(L)(S);	
				end loop;
			end loop;

			for L in LINES_BUFFER_DEPTH+1 to FIR_WIDTH-1 loop
				for S in 0 to PAD_LINE_WIDTH-1 loop
					linepad_buf(L)(S)<=(others=>'0');
				end loop;
			end loop;
		end if;


		--------------------------------------------
		if (cycle_shift_lines='1') then
		--------------------------------------------
			for L in 0 to FIR_WIDTH-1 loop
				for S in 0 to PAD_LINE_WIDTH-2 loop
					linepad_buf(L)(S)<=linepad_buf(L)(S+1);
				end loop;
				linepad_buf(L)(PAD_LINE_WIDTH-1)<=linepad_buf(L)(0);
			end loop;
		end if;
		--------------------------------------------
		if (linepad_add_line='1') then
		--------------------------------------------
			for L in 0 to 0 loop -- Hold First Line Only!!!!!
				 for P in 0 to N_PAD-1 loop
					linepad_buf(L)(P                 )<=(others=>'0'); -- here may be more interesting padding
					linepad_buf(L)(PAD_LINE_WIDTH-1-P)<=(others=>'0'); -- here may be more interesting padding
				end loop;
				
				 for S in FRAME_WIDTH-1 downto 0 loop
					linepad_buf(L)(S+N_PAD)<= hline_buf(S);	
				end loop;
			end loop;
			
			for L in 1 to FIR_WIDTH-1 loop -- Mad reshape, but's that's all for no-muxers
	    		for P in 0 to N_PAD-1 loop
					linepad_buf(L)(P                 )<=(others=>'0'); -- here may be more interesting padding
					linepad_buf(L)(PAD_LINE_WIDTH-1-P)<=(others=>'0'); -- here may be more interesting padding
				end loop;
				
				for S in 0 to FRAME_WIDTH-1 loop
					linepad_buf(L)(S+N_PAD)<=linepad_buf(L-1)( (S+N_PAD +2*N_PAD) mod PAD_LINE_WIDTH);
				end loop;
			end loop;
		end if;
	end if;
end process linepad_ctrl;

----------------------------------------------
-- FIR FILTER
---------------------------------------------
GEN_H:    for I in 0 to FIR_WIDTH-1 generate
    GEN_V:    for J in 0 to FIR_WIDTH-1 generate
    	fir_mat_din(I,J)			 <= linepad_buf(I)(J);
        fir_arr1d_din(I*FIR_WIDTH + J) <= fir_mat_din(I,J);-- Matrix to vector 
    end generate GEN_V;
end generate GEN_H;

GEN_ARR2VEC : for K in 0 to N_FIR_COEFF-1 generate
    fir_vec_din( (K+1)*BW_DIN-1 downto K*BW_DIN ) <= fir_arr1d_din(K);
end generate GEN_ARR2VEC;


fir_ivalid<= cycle_shift_lines;

convolution_unsigned : entity work.conv_uns
    generic map (
        BW_A     => BW_DIN,
        BW_B     => BW_FIR,
        BW_OUT   => BW_DIN,
        N_COEF   => N_FIR_COEFF,
        RND_BITS => 10
    )
    port map (
        clk     => clk,
        rst     => rst,
        i_valid => fir_ivalid,
        i_a     => fir_vec_din ,
        i_b     => i_fir_coef  ,
        o_res   => fir_out,
        o_valid => fir_ovalid
    );

process(clk) begin
	if rising_edge(clk) then
		v_sync_del<=v_sync_del(VSYNC_DEL-2 downto 0) & i_v_sync;
	end if;
end process;

fir_v_sync<= v_sync_del(VSYNC_DEL-1);
fir_h_sync<= fir_ovalid;
------------------------------------------------
-- Skip every even pixel and every even frame
-----------------------------------------------
frame_decimator_1 : entity work.frame_decimator
    generic map (
        BW_DIN => BW_DIN
    )
    port map (
        clk      => clk,
        rst      => rst,
      
        i_din    => fir_out,
        i_valid  => fir_ovalid,
        i_h_sync => fir_h_sync,
        i_v_sync => fir_v_sync,
      
        o_din    => o_din,
        o_valid  => o_valid,
        o_h_sync => o_h_sync,
        o_v_sync => o_v_sync
    );

end Behavioral;
