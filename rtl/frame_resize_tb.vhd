--------------------------------------------------------------------------------
-- Title       : FRAME RESIZER TESBENCH
-- Project     : Default Project Name
--------------------------------------------------------------------------------
-- File        : frame_resize_tb.vhd
-- Author      : User Name <user.email@user.company.com>
-- Company     : User Company Name
-- Created     : Fri Aug  3 17:35:48 2018
-- Last update : Fri Aug  3 23:34:31 2018
-- Platform    : Default Part Number
-- Standard    : <VHDL-2008 | VHDL-2002 | VHDL-1993 | VHDL-1987>
--------------------------------------------------------------------------------
-- Copyright (c) 2018 User Company Name
-------------------------------------------------------------------------------
-- Description: Testbench for frame resizer
--------------------------------------------------------------------------------
-- Revisions:  Revisions and documentation are controlled by
-- the revision control system (RCS).  The RCS should be consulted
-- on revision history.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;
use ieee.numeric_std.all;
use std.env.all;
use work.utils_pack.all;

-----------------------------------------------------------

entity frame_resize_tb is

end entity frame_resize_tb;

-----------------------------------------------------------

architecture testbench of frame_resize_tb is

	-- Testbench DUT generics as constants
    constant BW_DIN       : integer :=  16;
    constant BW_FIR       : integer :=   8;
    constant FIR_WIDTH    : integer :=   5;
    constant FRAME_WIDTH  : integer :=   8;
    constant FRAME_HEIGTH : integer :=   9;
    
    constant N_FIR_COEF : integer:=FIR_WIDTH*FIR_WIDTH;
    constant BW_FIR_VEC : integer:=N_FIR_COEF*BW_FIR;
    constant INT_MAX_COEFF : integer := 2**BW_FIR;


    subtype int_coef_t is integer range 0 to INT_MAX_COEFF-1;
    subtype coef_t      is std_logic_vector(BW_FIR-1 downto 0);
    type fir_mat_int_t  is array (FIR_WIDTH-1 downto 0, FIR_WIDTH-1 downto 0) of int_coef_t;                         
    type fir_mat_t      is array (FIR_WIDTH-1 downto 0, FIR_WIDTH-1 downto 0) of     coef_t;
    type fir_vec_t      is array (FIR_WIDTH*FIR_WIDTH-1             downto 0) of     coef_t;

	-- Testbench DUT ports as signals
    signal clk      : STD_LOGIC;
    signal rst      : STD_LOGIC;
    signal i_din    : STD_LOGIC_VECTOR (BW_DIN-1 downto 0);
    signal i_valid  : STD_LOGIC;
    signal i_h_sync : STD_LOGIC;
    signal i_v_sync : STD_LOGIC;
    signal o_din    : STD_LOGIC_VECTOR (BW_DIN-1 downto 0);
    signal o_valid  : STD_LOGIC;
    signal o_h_sync : STD_LOGIC;
    signal o_v_sync : STD_LOGIC;

    signal fir_mat_int  : fir_mat_int_t:= ( 
                ( 10 , 20 ,  30 , 20 , 10 ),
                ( 20 , 40 ,  60 , 40 , 20 ),
                ( 30 , 60 , 200 , 60 , 30 ),
                ( 20 , 40 ,  60 , 40 , 20 ),
                ( 10 , 20 ,  30 , 20 , 10 )
     );
    signal fir_mat      : fir_mat_t;
    signal fir_vec      : fir_vec_t;

    signal i_fir_coef : STD_LOGIC_VECTOR(BW_FIR_VEC-1 downto 0);

	-- Other constants
	constant C_CLK_PERIOD : real := 10.0e-9; -- NS

    signal tx_running : STD_LOGIC;

function uint2vec(x,bw: in integer) return std_logic_vector is
variable rez: std_logic_vector(bw-1 downto 0);
begin
    rez := std_logic_vector(to_unsigned(x, bw));
    return rez;
end function uint2vec;

begin
	-----------------------------------------------------------
	-- Clocks and Reset
	-----------------------------------------------------------
	CLK_GEN : process
	begin
		clk <= '1';
		wait for C_CLK_PERIOD / 2.0 * (1 SEC);
		clk <= '0';
		wait for C_CLK_PERIOD / 2.0 * (1 SEC);
	end process CLK_GEN;

	RESET_GEN : process
	begin
		rst <= '1',
		         '0' after 20.0*C_CLK_PERIOD * (1 SEC);
		wait;
	end process RESET_GEN;

-----------------------------------------------------------
-- Convert matrix of stimulus to STD_LOGIC_VECTOR
-----------------------------------------------------------
GEN_H:    for I in 0 to FIR_WIDTH-1 generate
    GEN_V:    for J in 0 to FIR_WIDTH-1 generate
        fir_mat(I,J)             <= uint2vec(fir_mat_int(I,J), BW_FIR) ;-- Integers to vectors
        fir_vec(I*FIR_WIDTH + J) <= fir_mat(I,J);-- Matrix to vector 
    end generate GEN_V;
end generate GEN_H;
GEN_ARR2VEC : for K in 0 to N_FIR_COEF-1 generate
    i_fir_coef( (K+1)*BW_FIR-1 downto K*BW_FIR ) <= fir_vec(K);
end generate GEN_ARR2VEC;
----------------------------------------------------------------------------------------
-- Generate Input Frame. By default - as counter, may be read from file later
----------------------------------------------------------------------------------------

frame_sender: process (clk) 
variable cnt_h      , 
         cnt_v      , 
         frame_cnt  : natural :=0;
variable pixel              : natural:=0;
begin
    if rising_edge(clk) then
        if tx_running='1' then
            if (cnt_h = FRAME_WIDTH) then
                cnt_h:=0;
                i_valid<='0';
                i_h_sync<='0';
                if (cnt_v=FRAME_HEIGTH-1) then
                    cnt_v:=0;
                    i_v_sync<='0';
                    frame_cnt := frame_cnt+1;
                else
                    cnt_v:=cnt_v+1;
                end if;
            else 
                i_din    <= uint2vec(pixel, BW_DIN);
                pixel    := pixel+1; -- Here should be something like frames(frame_cnt, cnt_h, cnt_v)
                i_valid  <='1';
                i_v_sync <='1';
                i_h_sync <='1';
                cnt_h :=cnt_h+1;
            end if;
        end if;
        if rst='1' then
            i_valid   <= '0';
            i_v_sync  <= '0';
            i_h_sync  <= '0';
            cnt_h     :=  0 ;
            cnt_v     :=  0 ;
            frame_cnt :=  0 ;
            pixel     :=  0 ;
        end if; 
    end if;
end process frame_sender;

main: process begin
    report " ** Start ";
    wait for 1 us;
    tx_running<='1';
    wait for 10 us;
    tx_running<='0';

    report " 31 => clog2 => " & integer'image(clog2(31));
    report " 32 => clog2 => " & integer'image(clog2(32));
    report " 33 => clog2 => " & integer'image(clog2(33));

    report " ** Done ";
    stop(0);
end process;
	-----------------------------------------------------------
	-- Entity Under Test
	-----------------------------------------------------------
    DUT : entity work.frame_resize
        generic map (
            BW_DIN       => BW_DIN,
            BW_FIR		 => BW_FIR,
            BW_FIR_VEC   => BW_FIR_VEC,
            FIR_WIDTH    => FIR_WIDTH,
            FRAME_WIDTH  => FRAME_WIDTH,
            FRAME_HEIGTH => FRAME_HEIGTH
        )
        port map (
            clk      => clk,
            rst      => rst,

            i_din    => i_din,
            i_valid  => i_valid,
            i_h_sync => i_h_sync,
            i_v_sync => i_v_sync,
            
            i_fir_coef => i_fir_coef,

            o_din    => o_din,
            o_valid  => o_valid,
            o_h_sync => o_h_sync,
            o_v_sync => o_v_sync
        );

end architecture testbench;