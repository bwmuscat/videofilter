
library IEEE;
use IEEE.std_logic_1164.all;
use ieee.STD_LOGIC_ARITH.all;
use work.utils_pack.all;

entity rnd_sat_u is
  	generic (
  			BW_DIN 			: integer:=  16 ;
   			BW_OUT          : integer:=  12 ;
		  	RND_BITS        : integer:=   10 
 	);
    Port ( 	clk : in STD_LOGIC;
           	rst : in STD_LOGIC;
           	
          	i_din   : in  STD_LOGIC_VECTOR  (BW_DIN-1 downto 0);
            o_dout  : out STD_LOGIC_VECTOR( BW_OUT -1 downto 0)
	 	);
end entity;