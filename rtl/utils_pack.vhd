
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;
use ieee.numeric_std.all;

package utils_pack is
	function uint2vec(x,bw: in integer) return std_logic_vector;
	function clog2( i : natural) return integer;
end package utils_pack;

package body utils_pack is
	function uint2vec(x,bw: in integer) return std_logic_vector is
	variable rez: std_logic_vector(bw-1 downto 0);
	begin
	    rez := std_logic_vector(to_unsigned(x, bw));
	    return rez;
	end function uint2vec;


	function clog2( i : natural) return integer is
    variable temp    : integer := i;
    variable ret_val : integer := 1; 
  begin         
    while temp > 1 loop
      ret_val := ret_val + 1;
      temp    := temp / 2;     
    end loop;
    
    return ret_val;
  end function;

end package body utils_pack;