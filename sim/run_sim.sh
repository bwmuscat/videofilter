vdel -lib work -all

vlib work


vmap work $PWD/work
rtl=../rtl
vhdlList=(
	$rtl/utils_pack.vhd
	$rtl/adder_tree.vhd
	$rtl/conv_uns.vhd
	$rtl/frame_decimator.vhd
	$rtl/frame_resize.vhd
	$rtl/frame_resize_tb.vhd
)

for src in "${vhdlList[@]}"
do
	vcom $src -2008 | grep '\*\*'
done

vsim -t ns -novopt work.frame_resize_tb -do "log -r /*; set StdArithNoWarnings 1; set NumericStdNoWarnings 1; ; run 12 us; exit;"



